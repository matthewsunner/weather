# NWS Weather Scraper

# WeatherScraper. 2018. Matt Sunner
# v2.0 - Detailed Weather Reports

This script allows the user to pull current and future NWS weather data from the website weather.gov using the command prompt. 

Please note: When using chromedriver.exe, be sure to place this in either the PATH, or the working repo. In the published code, it is setup to use the working folder as the path. 

If you have any questions about using this code, please do not hesitate to reach out. 

