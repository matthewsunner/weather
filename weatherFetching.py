# WeatherScraper. 2018. Matt Sunner
# v2.0 - Detailed Weather Reports

from bs4 import BeautifulSoup
import requests
import lxml
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait



print('Please enter the City & State (City, ST): ')
cityState = input(' ')

options = Options()
options.add_argument("--headless") 
options.add_argument('--no-sandbox') 
options.add_argument('--disable-gpu')
options.add_argument('start-maximized')
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")
# Update the below path to match the path of the cloned repo
driver = webdriver.Chrome(options=options, executable_path=r'C:\Users\sunne\Documents\Coding\Projects\weather\chromedriver.exe')
driver.get("http://weather.gov")

search = driver.find_element_by_name('inputstring')
search.click()

search.send_keys(cityState)
time.sleep(2)
search.send_keys(u'\ue007')
time.sleep(5)
url = driver.current_url
site = requests.get(url).text
weather_soup = BeautifulSoup(site, 'lxml')

weather = weather_soup.find('div', class_='pull-left')

future_weather_label = weather_soup.find('div', class_='col-sm-2 forecast-label')



label = weather_soup.find('p', class_='myforecast-current').text
temp = weather_soup.find('p', class_='myforecast-current-lrg').text

weather_data = []
for td in weather_soup.findAll('td'):
    weather_data.append(td.text)
# Current Humidity
humidity_label = weather_data[0]
humidity_value = weather_data[1]

# Current Wind Conditions
wind_label = weather_data[2]
wind_value = weather_data[3]

# Current Windchill
wc_label = weather_data[10]
wc_value = weather_data[11]


weather_data_future_label = []
for td in weather_soup.findAll('div', class_='col-sm-2 forecast-label'):
    weather_data_future_label.append(td.text)


weather_data_future_value = []
for td in weather_soup.findAll('div', class_='col-sm-10 forecast-text'):
    weather_data_future_value.append(td.text)


period1 = weather_data_future_label[0]
period1_description = weather_data_future_value[0]

period2 = weather_data_future_label[1]
period2_description = weather_data_future_value[1]

period3 = weather_data_future_label[2]
period3_description = weather_data_future_value[2]

period4 = weather_data_future_label[3]
period4_description = weather_data_future_value[3]

period5 = weather_data_future_label[4]
period5_description = weather_data_future_value[4]

period6 = weather_data_future_label[5]
period6_description = weather_data_future_value[5]

period7 = weather_data_future_label[6]
period7_description = weather_data_future_value[6]

period8 = weather_data_future_label[7]
period8_description = weather_data_future_value[7]

period9 = weather_data_future_label[8]
period9_description = weather_data_future_value[8]

period10 = weather_data_future_label[9]
period10_description = weather_data_future_value[9]


print('The Current Temperature is: ' + temp + '. The windchill is: ' + wc_value + '.')
print(' ')
print('Upcoming Forecast')
print(period1 + ': ' + period1_description)
print(period2 + ': ' + period2_description)
print(period3 + ': ' + period3_description)
print(period4 + ': ' + period4_description)
print(period5 + ': ' + period5_description)
print(period6 + ': ' + period6_description)
print(period7 + ': ' + period7_description)
print(period8 + ': ' + period8_description)
print(period9 + ': ' + period9_description)
print(period10 + ': ' + period10_description)
